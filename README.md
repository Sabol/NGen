##NGen
#Java name generator

##TODO:
1. Make it a command line tool (Maybe port it to c or python).
2. Add the option to specify what types of names to generate.
3. Refine the algorithm to handle punctuation in names.
4. Add an option for NGen to begin adding the names it generates to a file to be used as a seed for subsequent runs.
