import java.util.Random;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Vector;
import java.io.*;

/**
* <h1>NameGenerator</h1>
* <p>
* The NameGenerator class implements a Markov chain
* to randomly generate a name based on a seed text corpus
* of names. The class could work with any list of words
* however not just names.
* </p>
*
* @author  Cory Sabol
* @version 0.1
* @since   2014-12-03 
*/
public class NameGenerator {
	// Hashtable to hold key pairs and the letters that occur after each pair in
	// the text corpus.
	// Takes a string for the key and a vector of strings as the key value.
	Hashtable<String, Vector<String>> probTable = new Hashtable<String, Vector<String>>();
	private Random rnd = new Random();

	private File infile;
	private Scanner inf;

	// Alphabet to compare pair combinations against
	private String[] alphabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i",
			"j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
			"w", "x", "y", "z" };

	// Sample name text corpus.
	private String textCorpus = "Ben " + "Cory " + "Austin " + "Mark " + "Josh " + "Thomas " + "Ricky " + "Sarah " + "Daniel "
			+ "Martin " + "Ashton " + "Claire " + "Brittanay ";

	/**
	 * <p>
	 * Method to generate a random name
	 * </p>
	 * 
	 * @param seed - seed text
	 * @return String
	 * @throws IOException 
	 */
	public String generateName() throws IOException {
		textCorpus = seedGenerator();
		/*
		 * Firstly the input text needs to be tokenized into an array of words
		 * and those words need to be processed one at a time, so that
		 * whitespace issues that keep arising can be simply avoided.
		 */

		String name = "";
		String[] seedTokens;
		String[] pairsArray;

		seedTokens = tokenize(textCorpus);
		pairsArray = findPairs(seedTokens);

		for (int i = 0; i < pairsArray.length; i++) {
			findTails(pairsArray[i], textCorpus);
		}

		// Chain together a name from the probability table.
		name = chainName(pairsArray, textCorpus);

		return name;
	}
	
	/** 
	 * <p>
	 * Creates a text seed from the SeedNames.txt file, if the
	 * file is not found it will just use the default text corpus.
	 * The text file MUST follow the format of the included
	 * SeedNames.txt
	 * Do not include any miscellaneous characters such as , - + * etc.
	 * </p>
	 * @throws FileNotFoundException 
	 */
	public String seedGenerator() throws FileNotFoundException {
		String seed = "";
		infile = new File("SeedNames.txt");
		
		if (infile.exists()) {
			//System.out.println("Using seed file.");
			inf = new Scanner(infile);
			while (inf.hasNext()) {
				seed += inf.nextLine() + " ";
			}
		}
		else {
			//System.out.println("Using default seed.");
			seed = textCorpus;
		}
		return seed;
	}

    /**
     * <p>
     * Method to tokenize any string into individual words
     * </p>
     * @param str - String
     * @return String[]
     */
	private String[] tokenize(String str) {
		String delim = "[ \n]";
		String[] tokens = str.split(delim);
		return tokens;
	}

	/**
	 * <p>
	 * Method to create pairs of letters from an array of words
	 * </p>
	 * @param tokens
	 * @return
	 */
	private String[] findPairs(String[] tokens) {
		String[] pairsArray = null;
		String pairs = "";

		for (int i = 0; i < tokens.length; i++) {
			for (int j = 0; j < tokens[i].length() - 1; j++) {
				pairs += tokens[i].substring(j, j + 2) + "\n";
			}
		}
		// return the pairs split into tokens
		pairsArray = tokenize(pairs);
		return pairsArray;
	}

	/**
	 * <p>
	 * Method to find the possible letters that could occur
	 * after a given pair in the seed text.
	 * </p>
	 * @param pair - String Two letter pair
	 * @param seed - String seed text
	 */
	private void findTails(String pair, String seed) {
		Vector<String> tails = new Vector<String>();

		for (int i = 0; i < alphabet.length; i++) {
			if (seed.toLowerCase().contains(pair.toLowerCase() + alphabet[i])) {
				// If the string is found then simply add the letter of the
				// alphabet to tails
				// We can calculate the probability of each letter here too
				tails.add(alphabet[i]);
			}
		}

		// If no tails were found for a pair then it is an ending pair
		if (tails.isEmpty())
			tails.add("END");

		probTable.put(pair, tails);
	}

	/**
	 * <p>
	 * Method to put together a name from the previously created
	 * letter pairs and tail letters.
	 * </p>
	 * @param pairs - String[] array of letter pairs
	 * @param seed - String seed text
	 * @return - String
	 */
	private String chainName(String[] pairs, String seed) {
		String currentPair = "";
		String[] tails = {};
		String chosenTail = "";
		String name = "";
		boolean end = false;

		// get a randomly chosen starting pair
		currentPair = pairs[rnd.nextInt(pairs.length)];
		name += currentPair;
		tails = probTable.get(currentPair).toArray(new String[0]);

		while (!end) {
			chosenTail = tails[rnd.nextInt(tails.length)];

			if (chosenTail == "END" || name.length() > 6)
				end = true;
			else
				name += chosenTail;

			currentPair = name.length() > 2 ? name.substring(name.length() - 2)
					: currentPair;
			tails = probTable.get(currentPair).toArray(new String[0]);
		}

		// Capitalize the first letter of the name
		name = name.toLowerCase();
		name = name.substring(0, 1).toUpperCase() + name.substring(1);

		return name;
	}
}
