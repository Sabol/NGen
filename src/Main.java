import java.io.IOException;
import java.util.Scanner;


public class Main {
	public static void main( String args[] ) throws IOException {
		NameGenerator nGen= new NameGenerator();
		
		String name = "";
		boolean genAgain = true;
		
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Press 1 to generate a name 2 to quit:");
		
		do {
			if (kb.nextInt() == 1) {
				genAgain = true;
			} else {
				genAgain = false;
			}
			
			if (genAgain == true) {
				System.out.println("Name: " + nGen.generateName() + " " );
			}
		} while (genAgain);
		
		System.out.println("Exiting");
	}
}
